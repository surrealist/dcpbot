package lpv.core;

public class BotRuntimeException extends RuntimeException {

    public BotRuntimeException(
            final Throwable cause,
            final String format,
            final Object... params
    ) {
        super(String.format(format, params), cause);
    }

    public BotRuntimeException(
            final String format,
            final Object... params
    ) {
        super(String.format(format, params));
    }

    public BotRuntimeException(final Throwable cause) {
        super(cause);
    }
}
