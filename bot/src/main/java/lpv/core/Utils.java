package lpv.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Utils {

    public static Properties loadProperties(String fileName){
        final InputStream resource = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
        final Properties properties = new Properties();
        try {
            properties.load(resource);
        } catch (IOException e) {
            throw new BotRuntimeException(e);
        }
        return properties;
    }

}
