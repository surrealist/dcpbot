package lpv;

import com.github.ocraft.s2client.bot.S2Agent;
import com.github.ocraft.s2client.bot.gateway.UnitInPool;
import lpv.impl.MacroManager;
import lpv.impl.MapManager;
import lpv.impl.MoreObservations;

/**
 * 1) Главный орекстратор
 * 2) Следит за корректностью инициализации всех менеджеров (порядок и момент) */
public class DCPBot extends S2Agent {

    private MacroManager macroManager;
    private MapManager mapManager;
    private MoreObservations moreObservations;

    public DCPBot() {

    }


    @Override
    public void onGameStart() {
        System.out.println("Hello world of Starcraft II bots!");

        moreObservations = new MoreObservations(observation());
        mapManager = new MapManager(this);
        macroManager = new MacroManager(this, mapManager);

    }

    @Override
    public void onStep() {
        macroManager.onStep();
    }

    @Override
    public void onUnitIdle(UnitInPool unitInPool) {
        macroManager.onUnitIdle(unitInPool);
    }

    @Override
    public void onUnitCreated(UnitInPool unitInPool) {
        macroManager.onUnitCreated(unitInPool);

    }

    public void onBuildingConstructionComplete(UnitInPool unitInPool) {
        macroManager.onBuildingConstructionComplete(unitInPool);
        mapManager.onBuildingConstructionComplete(unitInPool);
    }

}
