package lpv;

import com.github.ocraft.s2client.bot.S2Coordinator;
import com.github.ocraft.s2client.protocol.game.BattlenetMap;
import com.github.ocraft.s2client.protocol.game.Difficulty;
import com.github.ocraft.s2client.protocol.game.Race;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lpv.core.Utils;

import javax.annotation.Nullable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;


public class GameStarter {

    private static final Logger log = LoggerFactory.getLogger(GameStarter.class);

    public static void main(String[] args) {

        Path execPath = getExecPath();
        S2Coordinator s2Coordinator = S2Coordinator.setup()
                .loadSettings(args)
                .setRealtime(false)
                .setProcessPath(execPath)
                .setMultithreaded(false)
                .setParticipants(
                        S2Coordinator.createParticipant(Race.TERRAN, new DCPBot()),
                        S2Coordinator.createComputer(Race.ZERG, Difficulty.EASY)

                )
                .launchStarcraft()
                .startGame(BattlenetMap.of("Deathaura LE"));


        //noinspection StatementWithEmptyBody
        while (s2Coordinator.update()) {}

        s2Coordinator.quit();
    }

    @Nullable
    private static Path getExecPath() {
        // Выделить в отдельный класс если будет много свойств
        final Properties configs = Utils.loadProperties("configs.properties");
        final String execPathString = (String) configs.get("executable.path");
        log.info("SC2 exec classpath {}", execPathString);

        return Strings.isNullOrEmpty(execPathString)?
                Paths.get(execPathString):
                null;
    }

}
