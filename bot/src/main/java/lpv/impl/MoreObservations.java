package lpv.impl;

import com.github.ocraft.s2client.bot.gateway.ObservationInterface;
import com.github.ocraft.s2client.bot.gateway.UnitInPool;
import com.github.ocraft.s2client.protocol.data.Ability;
import com.github.ocraft.s2client.protocol.unit.Alliance;

import java.util.function.Predicate;

public class MoreObservations {

    private ObservationInterface observation;

    public MoreObservations(ObservationInterface observation) {
        this.observation = observation;
    }

    public boolean AreWeAlreadyDoing(Ability abilitiy){
        return !observation.getUnits(Alliance.SELF, alreadyDoing(abilitiy)).isEmpty();
    }

    private Predicate<UnitInPool> alreadyDoing(Ability abilitiy) {

        return unitInPool -> unitInPool.unit()
                .getOrders()
                .stream()
                .anyMatch(unitOrder -> abilitiy.getAbilityId() == unitOrder.getAbility().getAbilityId());
    }
}
