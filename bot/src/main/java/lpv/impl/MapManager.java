package lpv.impl;

import com.github.ocraft.s2client.bot.S2Agent;
import com.github.ocraft.s2client.bot.gateway.ObservationInterface;
import com.github.ocraft.s2client.bot.gateway.UnitInPool;
import com.github.ocraft.s2client.protocol.data.Units;
import com.github.ocraft.s2client.protocol.spatial.Point;
import com.github.ocraft.s2client.protocol.spatial.Point2d;
import com.github.ocraft.s2client.protocol.unit.Alliance;
import com.github.ocraft.s2client.protocol.unit.Unit;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MapManager {

    private final List<Point> expansionLocations;
    private int expanded=1;
    private ObservationInterface observation;

    public MapManager(S2Agent s2Agent) {
        expansionLocations = s2Agent.query().calculateExpansionLocations(s2Agent.observation());
        observation = s2Agent.observation();
    }

    public Point2d findRandomPoint(Point center, float radius) {
        return observation.getGameInfo().findRandomLocation(
                center.toPoint2d().sub(radius, radius),
                center.toPoint2d().add(radius, radius)
        );
    }

    public Optional<Unit> findNearestMineralPile(Point2d start){
        double currentDistance = Double.MAX_VALUE;
        Optional<Unit> currentUnit = Optional.empty();

        for (UnitInPool unit : observation.getUnits(Alliance.NEUTRAL)) {
            if(!unit.unit().getType().equals(Units.NEUTRAL_MINERAL_FIELD)){
                continue;
            }
            double newDistance = unit.unit().getPosition().toPoint2d().distance(start);
            if(newDistance<currentDistance){
                currentDistance = newDistance;
                currentUnit = Optional.of(unit.unit());
            }
        }
        return currentUnit;
    }

    public void onBuildingConstructionComplete(UnitInPool unitInPool) {
        if(UnitInPool.isUnit(Units.TERRAN_COMMAND_CENTER).test(unitInPool)){
            expanded++;
        }
    }

    public boolean existsEmptyBaseLocation(){
        return expanded<expansionLocations.size();
    }

    public List<Point2d> getExpansionLocations(){
        return expansionLocations.stream().map(Point::toPoint2d).collect(Collectors.toList());
    }

}



