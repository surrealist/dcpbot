package lpv.impl;

import com.github.ocraft.s2client.bot.S2Agent;
import com.github.ocraft.s2client.bot.gateway.ActionInterface;
import com.github.ocraft.s2client.bot.gateway.ObservationInterface;
import com.github.ocraft.s2client.bot.gateway.QueryInterface;
import com.github.ocraft.s2client.bot.gateway.UnitInPool;
import com.github.ocraft.s2client.protocol.data.*;
import com.github.ocraft.s2client.protocol.game.raw.StartRaw;
import com.github.ocraft.s2client.protocol.response.ResponseGameInfo;
import com.github.ocraft.s2client.protocol.spatial.Point2d;
import com.github.ocraft.s2client.protocol.unit.Alliance;
import com.github.ocraft.s2client.protocol.unit.Tag;
import com.github.ocraft.s2client.protocol.unit.Unit;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class MacroManager {

    private final ObservationInterface observations;
    private final QueryInterface queries;
    private final ActionInterface actions;

    private final MapManager mapManager;
    private final MoreObservations moreObservations;

    private Set<Tag> builderTags = new HashSet<>();


    public MacroManager(S2Agent agent, MapManager mapManager) {
        observations = agent.observation();
        moreObservations = new MoreObservations(observations);
        queries = agent.query();
        actions = agent.actions();
        this.mapManager = mapManager;

    }

    // ----- Events
    public void onStep() {
        buildSupplyIfNeeded();
        buildCommandCenterIfNeeded();
        buildBarracksIfNeeded();
    }

    private void buildBarracksIfNeeded() {
        if (!WeDontNeedNewCommandCenter()) { // if we need CC -- ignore barracks
            return;
        }
        if (moreObservations.AreWeAlreadyDoing(Abilities.BUILD_BARRACKS)) {
            return;
        }
    //    System.out.println("we need barracks");

        if (!isEnoughResourcesFor(Units.TERRAN_BARRACKS)) {
            return;
        }
        buildStructure(Abilities.BUILD_BARRACKS, Collections.emptySet());
    }

    void buildStructure(Ability struct, Collection<Point2d> locationsVariants){
        Optional<Unit> notBuildingSCVOpt = findNotBuildingSCV();
        if(!notBuildingSCVOpt.isPresent()){
            System.out.println("Builder not found");
            return;
        }
        Unit builder = notBuildingSCVOpt.get();

        Point2d locationToPut = null;
        if(locationsVariants.isEmpty()) {


            for (int tries = 0; tries < 20; tries++) {

                Point2d placementPoint = mapManager.findRandomPoint(builder.getPosition(), tries + 5);

                if (queries.placement(struct, placementPoint)) {
                    locationToPut = placementPoint;
                    break;
                }

            }
        } else{
            for (Point2d locationsVariant : locationsVariants) {
                if (queries.placement(struct, locationsVariant)) {
                    locationToPut = locationsVariant;
                    break;
                }
            }
        }


        if(locationToPut!=null) {
            actions.unitCommand(
                    builder,
                    struct,
                    locationToPut,
                    false
            );
            System.out.printf("Queried command %s at location %s %n by worker %s",
                    ((Abilities)struct).name(),
                    locationToPut,
                    builder.getTag()
            );
            builderTags.add(builder.getTag());
        }else{
            System.out.println("Couldn't found placement for "+((Abilities)struct).name());
        }

    }


    public void onUnitIdle(UnitInPool unitInPool) {
        Unit unit = unitInPool.unit();
        switch (((Units) unit.getType())) {
            case TERRAN_COMMAND_CENTER:
                if (unit.getAssignedHarvesters().orElse(0) <= unit.getIdealHarvesters().orElse(0)) {
                    actions.unitCommand(unit, Abilities.TRAIN_SCV, false);
                }
                break;
            case TERRAN_SCV:
                System.out.println(String.format("Builder %s released",unit.getTag()));
                //builderTags.remove(unit.getTag());
                builderTags.clear();

                Optional<Unit> nearestMineralPile = mapManager.findNearestMineralPile(unit.getPosition().toPoint2d());
                nearestMineralPile.ifPresent(value -> actions.unitCommand(unit, Abilities.SMART, value, false));


            case TERRAN_BARRACKS:
                actions.unitCommand(unit, Abilities.TRAIN_MARINE, false);
        }


    }

    public void onBuildingConstructionComplete(UnitInPool unitInPool) {
        if (UnitInPool.isUnit(Units.TERRAN_SUPPLY_DEPOT).test(unitInPool)) {
        }
    }


    // Implementation

    public boolean isEnoughResourcesFor(UnitType unitType) {
        UnitTypeData unitTypeData = this.observations.getUnitTypeData(false).get(unitType);

        return observations.getMinerals() >= unitTypeData.getMineralCost().orElse(0) &&
                observations.getVespene() >= unitTypeData.getVespeneCost().orElse(0);

    }


    private void buildCommandCenterIfNeeded() {
        if (WeDontNeedNewCommandCenter()) {
            return;
        }

      //  System.out.println("We need CC");

        if (!isEnoughResourcesFor(Units.TERRAN_COMMAND_CENTER)) {
            return;
        }

        buildStructure(Abilities.BUILD_COMMAND_CENTER, mapManager.getExpansionLocations());
    }

    private boolean WeDontNeedNewCommandCenter() {
        if (moreObservations.AreWeAlreadyDoing(Abilities.BUILD_COMMAND_CENTER)) {
            return true;
        }

        if (!mapManager.existsEmptyBaseLocation()) {
            return true;
        }

        boolean isThereAnyFedUpBase = false;
        for (UnitInPool unit : observations.getUnits(Alliance.SELF, UnitInPool.isUnit(Units.TERRAN_COMMAND_CENTER))) {
            Unit commandCenter = unit.unit();

            // Немного заранее сигнализируем о перенасыщении
            if (commandCenter.getAssignedHarvesters().orElse(0) + 2 >= commandCenter.getIdealHarvesters().orElse(0)) {
                isThereAnyFedUpBase = true;
            }
        }
        return isThereAnyFedUpBase;
    }


    private void buildSupplyIfNeeded() {

        if (observations.getFoodCap() >= observations.getFoodUsed() + 2 || observations.getMinerals() < 100) {
            return;
        }

        if (moreObservations.AreWeAlreadyDoing(Abilities.BUILD_SUPPLY_DEPOT)) {
            return;
        }

        buildStructure(Abilities.BUILD_SUPPLY_DEPOT, Collections.emptySet());
    }

    private Optional<Unit> findNotBuildingSCV() {

        return observations
                .getUnits(Alliance.SELF, UnitInPool.isUnit(Units.TERRAN_SCV))
                .stream()
                .filter(unitInPool -> !builderTags.contains(unitInPool.getTag()))
                .map(UnitInPool::unit)
                .findAny()
        ;

        /*List<UnitInPool> units = observations.getUnits(Alliance.SELF, UnitInPool.isUnit(Units.TERRAN_SCV));

        for (UnitInPool unitInPool : units) {
            Unit unit = unitInPool.unit();

            if (
                    unit.getOrders().stream()

                            .allMatch(uo ->
                            uo.getAbility().equals(Abilities.HARVEST_GATHER) || uo.getAbility().equals(Abilities.HARVEST_RETURN)
                    )
            ) {
                return Optional.of(unit);
            }

        }


        return Optional.empty();*/
    }


    int marineCounter = 0;
    int waveCounter = 1;
    public void onUnitCreated(UnitInPool unitInPool) {
        Unit unit = unitInPool.unit();
        switch (((Units)unit.getType())){
            case TERRAN_MARINE:
                marineCounter++;

                if (marineCounter>=10*waveCounter){
                    List<UnitInPool> units = observations.getUnits(Alliance.SELF, UnitInPool.isUnit(Units.TERRAN_MARINE));
                    Point2d point2d = findEnemyPosition().orElse(Point2d.of(20, 20));
                    for (UnitInPool inPool : units) {
                        actions.unitCommand(inPool.unit(),Abilities.ATTACK_ATTACK, point2d, false);
                    }
                    waveCounter++;
                    if(waveCounter>2){
                        waveCounter=2;
                    }
                }
                break;
            default:
                break;
        }

    }

    private Optional<Point2d> findEnemyPosition() {
        ResponseGameInfo gameInfo = observations.getGameInfo();

        Optional<StartRaw> startRaw = gameInfo.getStartRaw();
        if (startRaw.isPresent()) {
            Set<Point2d> startLocations = new HashSet<>(startRaw.get().getStartLocations());
            startLocations.remove(observations.getStartLocation().toPoint2d());
            if (startLocations.isEmpty()) return Optional.empty();
            return Optional.of(new ArrayList<>(startLocations)
                    .get(ThreadLocalRandom.current().nextInt(startLocations.size())));
        } else {
            return Optional.empty();
        }
    }
}
